<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//Homework 1
Route::get('hello/{name?}', 'HelloController@Hello');
Route::get('broevi', 'BroeviController@Broevi');
//Homework 2
Route::get('homework2/{name?}', 'UserController@Name');
//Homework 3
Route::get('homework3', "Homework3Controller@form")->name('formData');
Route::post('h3Info', "Homework3Controller@formInfo")->name('formInfo');

//Homework 4
Route::get('homework4', "Homework4Controller@form")->name('formDataH4');
Route::post('h4Info', "Homework4Controller@formInfo")->name('formInfoH4');

//Homework 5
Route::get('homework5/home', "Homework5Controller@home")->name('HomeH5');
Route::get('homework5/aboutus', "Homework5Controller@aboutus")->name('AboutUs');
Route::get('homework5/contact', "Homework5Controller@contact")->name('Contact');

//Homework 6
Route::get('homework6/index', "Homework6Controller@index")->name('HomeH6index');
Route::get('homework6/create', "Homework6Controller@create")->name('HomeH6create');
Route::get('homework6/show/{id}', "Homework6Controller@show")->name('HomeH6show');
Route::get('homework6/delete/{id}', "Homework6Controller@destroy")->name('HomeH6destroy');
Route::get('homework6/edit/{id}', "Homework6Controller@edit")->name('HomeH6edit');
Route::post('homework6/store', "Homework6Controller@store")->name('HomeH6store');
Route::post('homework6/update', "Homework6Controller@update")->name('HomeH6update');

//Homework 7
Route::get('homework7/index', "Homework7Controller@index")->name('HomeH7index');
Route::get('homework7/create', "Homework7Controller@create")->name('HomeH7create');
Route::get('homework7/show/{id}', "Homework7Controller@show")->name('HomeH7show');
Route::get('homework7/delete/{id}', "Homework7Controller@destroy")->name('HomeH7destroy');
Route::get('homework7/edit/{id}', "Homework7Controller@edit")->name('HomeH7edit');
Route::post('homework7/store', "Homework7Controller@store")->name('HomeH7store');
Route::post('homework7/update', "Homework7Controller@update")->name('HomeH7update');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Homework Auth
Route::get('register', [
    'as' => 'register',
    'uses' => 'Auth\RegisterController@testfunction'
  ]);

//   Route::post('auth.create', [
//     'as' => 'create',
//     'uses' => 'Auth\RegisterController@create'
//   ]);

