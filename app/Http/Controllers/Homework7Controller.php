<?php

namespace App\Http\Controllers;
use App\eGenre;
use App\eMovie;

use Illuminate\Http\Request;

class Homework7Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $movies = eMovie::join('e_genres', 'e_movies.genre_id', '=', 'e_genres.id')
        ->select('e_movies.*', 'e_genres.name')
        ->paginate(5);

        return view('homework7.index', ["movies" => $movies]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $genres = eGenre::all();

        return view('homework7.create', ["genres" => $genres]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $movies = new eMovie();
        $movies->title = $request->get('title');
        $movies->description = $request->get('description');
        $movies->date_released = $request->get('date_released');
        $movies->genre_id = $request->get('genre_id');
        $movies->save();

        return redirect()->route("HomeH7index");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $movie = eMovie::join('e_genres', 'e_movies.genre_id', '=', 'e_genres.id')
        ->select('e_movies.*', 'e_genres.name')
        ->where('e_movies.id', $id)->first();

        return view('homework7.show', ["movie" => $movie]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $movie = eMovie::join('e_genres', 'e_movies.genre_id', '=', 'e_genres.id')
        ->select('e_movies.*', 'e_genres.name')
        ->where('e_movies.id', $id)->first();
        $genres = eGenre::all();
        return view('homework7.edit', ["movie" => $movie, "genres" => $genres]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $movies = eMovie::find($request->id);
        $movies->title = $request->title;
        $movies->description = $request->description;
        $movies->date_released = $request->date_released;
        $movies->genre_id = $request->genre_id;
        $movies->save();

        return redirect()->route("HomeH7index");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        eMovie::where('id', $id)->delete();
        return redirect()->route("HomeH7index");
    }
}
