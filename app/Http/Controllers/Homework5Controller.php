<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Homework5Controller extends Controller
{
    public function home() {
        return view('homework5.home');
    }
    public function aboutus() {
        return view('homework5.aboutus');
    }
    public function contact() {
        return view('homework5.contact');
    }
}
