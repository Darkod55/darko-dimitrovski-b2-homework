<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Homework3Controller extends Controller
{
    public function form() {
        return view('homework3');
    }
    public function formInfo(Request $request) {
        $session_array = $request->all();  
        // dd($session_array);
        return view('h3Info', ["data" => $session_array]);
    }
}
