<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Homework6Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        
        $movies = \DB::table('movies')
        ->join('genres', 'movies.genre_id', '=', 'genres.id')
        ->select('movies.*', 'genres.name')
        ->paginate(6);

        // dd($movies);
        return view('homework6.index', ['movies' => $movies]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    { 
        $genres = \DB::table('genres')->get();

        return view('homework6.create', ["genres" => $genres]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \DB::table('movies')->insert([
            "title" => $request->title,
            "description" => $request->description,
            "date_released" => $request->date_released,
            "genre_id" => $request->genre_id
        ]);
        return redirect()->route("HomeH6index");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $movie = \DB::table('movies')->join('genres', 'movies.genre_id', '=', 'genres.id')
        ->select('movies.*', 'genres.name')
        ->where('movies.id', $id)->first();

        return view('homework6.show', ["movie" => $movie]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $movie = \DB::table('movies')->join('genres', 'movies.genre_id', '=', 'genres.id')
        ->select('movies.*', 'genres.name')
        ->where('movies.id', $id)->first();
        $genres = \DB::table('genres')->get();
        return view('homework6.edit', ["movie" => $movie, "genres" => $genres]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        \DB::table('movies')
            ->where('id', $request->id)
            ->update(
                [
                    "title" => $request->title,
                    "description" => $request->description,
                    "date_released" => $request->date_released,
                    "genre_id" => $request->genre_id
                ]);
        return redirect()->route("HomeH6index");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::table('movies')->where('id', $id)->delete();
        return redirect()->route("HomeH6index");
    }
}
