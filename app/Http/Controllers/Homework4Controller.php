<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Homework4Controller extends Controller
{
    public function form() {
        return view('homework4');
    }

    public function formInfo(Request $request) {
        $request->validate(
            [
                "firstname" => 'required|min:2|alpha',
                "lastname" => 'required|min:2|alpha',
                "username" => 'required|regex:/^(?=.*?[A-Z])(?=.*?[0-9])/',
                "email" => 'required|email',
                "password" => 'required|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[?!@$%^&*-]).{6,}$/'
                
            ]
        ); 
        $session_array = $request->all();  
        // dd($session_array);
        return view('h4Info', ["data" => $session_array]);
    }
}