<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BroeviController extends Controller
{
    public function Broevi() {
        return view('broevi', ['numbers' => ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10']]);
    }
}
