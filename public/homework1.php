<?php
require "../vendor/autoload.php";

use Carbon\Carbon;

$mydate = new Carbon('1990-10-02 14:00:00');
$nowdate = Carbon::now();

echo "My birthdate - " . $mydate ."<br>";
echo "Current date - " . $nowdate."<br>";
$test = $mydate->diffInSeconds($nowdate);
$dt = Carbon::now()->addSecond($test);
$dt_old = Carbon::now();
$days = $dt->diffInDays($dt_old);
$dt = $dt->subDays($days);
$hours = $dt->diffInHours($dt_old);
$dt = $dt->subHours($hours);
$minutes = $dt->diffInMinutes($dt_old);  
echo "<br><br>"; 
echo "The difference between my birthday and the current date is " . $days .' days ' . $hours .' hours ' . $minutes.' minutes';
?>