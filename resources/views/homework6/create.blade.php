<!DOCTYPE html>
    <html>
        <head>
        <title>Homework 6</title>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- <link href="style.css" type="text/css" rel="stylesheet"> -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        </head>
        <body>
            <form method="POST" action="{{ route('HomeH6store') }}">
                @csrf
                <div class="form group">
                    <label for="title">Title</label><br />
                    <input type="text" id="title" name="title" />
                </div>
                <div class="form group">
                    <label for="description">Description</label><br />
                    <input type="text" id="description" name="description" />
                </div>
                <div class="form group">
                    <label for="date_released">Date Released</label><br />
                    <input type="date" id="date_released" name="date_released" />
                </div>
                <div class="form group">
                    <label for="genres">Genre</label><br />
                    <select name="genre_id">
                        @foreach($genres as $genre)
                            <option value="{{$genre->id}}">{{$genre->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form group">
                    <button type="submit" class="btn btn-success">Submit</button>
                </div>
            </form>
        </body>
    </html>