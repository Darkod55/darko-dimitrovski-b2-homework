<!DOCTYPE html>
    <html>
        <head>
        <title>Homework 6</title>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- <link href="style.css" type="text/css" rel="stylesheet"> -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        </head>
        <body>
        <h1>Movies</h1> 
        <a href="{{route('HomeH6create')}}">Add New Movie</a>
        <hr>
            @foreach($movies as $movie)
                <h3>Title - {{$movie->id}}</h4>
                <h3>Title - {{$movie->title}}</h4>
                <p>Description - {{$movie->description}}</p>
                <h4>Release Date - {{$movie->date_released}}</h4>
                <h4>Genre - {{$movie->name}}</h4>
                <button><a href="show/{{$movie->id}}">Show</a></button>
                <button><a href="edit/{{$movie->id}}">Edit</a></button>
                <button><a href="delete/{{$movie->id}}">Delete</a></button>
                <hr>
            @endforeach
            <div>
                {{$movies->links()}}
            </div>
        </body>
    </html>