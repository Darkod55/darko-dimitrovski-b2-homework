<!DOCTYPE html>
    <html>
        <head>
        <title>@yield('title', 'Default')</title>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- <link href="style.css" type="text/css" rel="stylesheet"> -->
        @include('layouts.style')
        @include('layouts.script')
        <style>
            .navbar-fixed-top {box-shadow: 0px 2px #00000042;}
            a {color: black;}
        </style>
        </head>
        <body>
        <header>
        <nav class="navbar nav-margin navbar-fixed-top">
            <div class="container-fluid top-header">
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-left">
                        <li><a href="{{ route('HomeH5') }}">Home</a></li>
                        <li><a href="{{ route('AboutUs') }}">About Us</a></li>
                        <li><a href="{{ route('Contact') }}">Contact</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    <div class="container-fluid" style="height: 92vh; display: flex; justify-content: center; align-items: center">
        <div class="row">
            <div class="col-md-12">
                @yield('pagecontent')
            </div>
        </div>
    </div>
    <footer>

    @include('layouts.footer5')

    </footer>
        </body>
    </html>