<!DOCTYPE html>
    <html>
        <head>
        <title>Homework 3</title>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <!-- Popper JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        </head>
        <body style="display: flex; align-items: center; justify-content: center; flex-direction: column;">
            <h1>Firstname - {{ $data['firstname'] }}</h1>
            <h1>Lastname - {{ $data['lastname'] }}</h1>
            <h1>Username - {{ $data['username'] }}</h1>
            <h1>Email - {{ $data['email'] }}</h1>
        </body>
    </html>