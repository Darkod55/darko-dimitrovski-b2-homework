<!DOCTYPE html>
    <html>
        <head>
        <title>Homework 7</title>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- <link href="style.css" type="text/css" rel="stylesheet"> -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        </head>
        <body>
            <a href="{{route('HomeH7index')}}">Back to all Movies</a>
            <hr>
            <h3>Title - {{$movie->title}}</h4>
            <p>Description - {{$movie->description}}</p>
            <h4>Release Date - {{$movie->date_released}}</h4>
            <h4>Genre - {{$movie->name}}</h4>
        </body>
    </html>