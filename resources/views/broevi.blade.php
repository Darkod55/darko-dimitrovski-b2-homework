<!DOCTYPE html>
    <html>
        <head>
        <title>Broevi</title>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- <link href="style.css" type="text/css" rel="stylesheet"> -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        </head>
        <body>
        <h1>Broevite se</h1>
        <ul>
            @foreach($numbers as $number)
                <li>{{ $number }} -
                @if($number % 2  == 0)
                    Paren
                @else Neparen
                @endif
                </li>
            @endforeach
        </ul>
        </body>
    </html>