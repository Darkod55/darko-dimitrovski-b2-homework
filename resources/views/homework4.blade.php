<!DOCTYPE html>
    <html>
        <head>
        <title>Homework 4</title>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <!-- Popper JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        </head>
        <body style="display: flex; align-items: center; justify-content: center; flex-direction: column; height: 100vh;">
        <span style="color: red">
            @if($errors->any())

                @foreach($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
            @endif
        </span>
            <form method="POST" action="{{ route('formInfoH4') }}">
                @csrf
                <div class="form group">
                    <label for="firstname">Firstname</label><br />
                    <input type="text" id="firstname" name="firstname" value="{{ old('firstname') }}" />
                </div>
                <div class="form group">
                    <label for="lastname">Lastname</label><br />
                    <input type="text" id="lastname" name="lastname" value="{{ old('lastname') }}" />
                </div>
                <div class="form group">
                    <label for="username">Username</label><br />
                    <input type="text" id="username" name="username" value="{{ old('username') }}" />
                </div>
                <div class="form group">
                    <label for="email">Email</label><br />
                    <input type="text" id="email" name="email" value="{{ old('email') }}" />
                </div>
                <div class="form group">
                    <label for="password">Password</label><br />
                    <input type="password" id="password" name="password" /><br />
                </div>
                <div class="form group">
                    <button type="submit" class="btn btn-success">Submit</button>
                </div>
            </form>
        </body>
    </html>