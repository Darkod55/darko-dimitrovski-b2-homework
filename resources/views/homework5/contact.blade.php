@extends('../layouts.master5')

@section('title')
Contact
@endsection

@section('pagecontent')
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <h3>Contact us:</h3>
            <form>
                @csrf
                <div class="form group">
                    <label for="email">Email Address</label><br />
                    <input type="text" class="form-control" id="email" name="email" placeholder="Email" />
                </div>
                <div class="form group">
                    <label for="subject">Subject</label><br />
                    <input type="text" class="form-control" id="subject" name="subject" placeholder="Subject" />
                </div>
                <div class="form group">
                    <label for="message">Message</label><br />
                    <textarea id="message" class="form-control" name="message" placeholder="Message"></textarea>
                </div>
                <br />
                <div class="form group">
                    <button type="submit" class="btn btn-success">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection