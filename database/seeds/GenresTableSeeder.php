<?php

use Illuminate\Database\Seeder;

class GenresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('genres')
            ->insert([
                ['name' => "Adventure"],
                ['name' => "Action"],
                ['name' => "Comedy"],
                ['name' => "Fantasy"],
                ['name' => "Mystery"],
                ['name' => "Horror"]
            ]);
        }
}
