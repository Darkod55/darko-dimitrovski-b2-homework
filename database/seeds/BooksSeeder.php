<?php

use Illuminate\Database\Seeder;

class BooksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')
            ->insert([
                ['title' => "My Book", 'isbn' => "2233", 'cost' => "18.2", "author_id" => "1", "publisher_name" => "Matica"],
                ['title' => "My Book2", 'isbn' => "22133", 'cost' => "23.2", "author_id" => "2", "publisher_name" => "Prosvetno Delo"],
            ]);
        }
    }
