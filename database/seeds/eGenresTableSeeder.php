<?php

use Illuminate\Database\Seeder;

class eGenresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('e_genres')
            ->insert([
                ['name' => "Adventure"],
                ['name' => "Action"],
                ['name' => "Comedy"],
                ['name' => "Fantasy"],
                ['name' => "Mystery"],
                ['name' => "Horror"]
            ]);
        }
}
