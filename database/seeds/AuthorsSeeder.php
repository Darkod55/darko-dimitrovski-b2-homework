<?php

use Illuminate\Database\Seeder;

class AuthorsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('authors')
            ->insert([
                ['name' => "Darko", 'email' => "darkod55@yahoo.com", 'location' => "Stip"],
                ['name' => "Stefan", 'email' => "stefan@yahoo.com", 'location' => "Skopje"]
            ]);
        }
    }
