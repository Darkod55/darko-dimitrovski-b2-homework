<?php

use Illuminate\Database\Seeder;
use Faker\Factory;

class MoviesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        DB::table('movies')
            ->insert([
                ['title' => "Avengers", 'description' => "This is description for The Avengers", 'date_released' => $faker->date, 'genre_id' => $faker->numberBetween($min = 1, $max = 6)],
                ['title' => "King Kong", 'description' => "This is description for King Kong", 'date_released' => $faker->date, 'genre_id' => $faker->numberBetween($min = 1, $max = 6)],
                ['title' => "Star Wars", 'description' => "This is description for Star Wars", 'date_released' => $faker->date, 'genre_id' => $faker->numberBetween($min = 1, $max = 6)],
                ['title' => "Batman", 'description' => "This is description for Batman", 'date_released' => $faker->date, 'genre_id' => $faker->numberBetween($min = 1, $max = 6)],
                ['title' => "Joker", 'description' => "This is description for Joker", 'date_released' => $faker->date, 'genre_id' => $faker->numberBetween($min = 1, $max = 6)],
                ['title' => "Jumanji", 'description' => "This is description for Jumanji", 'date_released' => $faker->date, 'genre_id' => $faker->numberBetween($min = 1, $max = 6)],
                ['title' => "Frozen", 'description' => "This is description for Frozen", 'date_released' => $faker->date, 'genre_id' => $faker->numberBetween($min = 1, $max = 6)],
                ['title' => "Terminator", 'description' => "This is description for Terminator", 'date_released' => $faker->date, 'genre_id' => $faker->numberBetween($min = 1, $max = 6)],
                ['title' => "Aquaman", 'description' => "This is description for Aquaman", 'date_released' => $faker->date, 'genre_id' => $faker->numberBetween($min = 1, $max = 6)],
                ['title' => "Lion King", 'description' => "This is description for Lion King", 'date_released' => $faker->date, 'genre_id' => $faker->numberBetween($min = 1, $max = 6)],
                ['title' => "Rambo", 'description' => "This is description for Rambo", 'date_released' => $faker->date, 'genre_id' => $faker->numberBetween($min = 1, $max = 6)],
                ['title' => "Superman", 'description' => "This is description for Superman", 'date_released' => $faker->date, 'genre_id' => $faker->numberBetween($min = 1, $max = 6)],
            ]);
    }
}
